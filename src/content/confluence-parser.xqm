xquery version "3.1";
(:~
 : Parser for Confluence's REST interface.
 : @author Markus Matoni
 : @author Johannes Biermann
 : @author Mathias Göbel
 : @author Stefan Hynek
 : @version 0.3
:)

module namespace conpa="http://sepia.io/conpa/confluence-parser";
import module namespace config="https://sade.textgrid.de/ns/config" at "../config.xqm";
import module namespace app="https://sade.textgrid.de/ns/app" at "../app.xqm";
import module namespace functx="http://www.functx.com";
import module namespace console="http://exist-db.org/xquery/console";

declare namespace cf="https://sade.textgrid.de/ns/configfile";
declare namespace http="http://expath.org/ns/http-client";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare variable $confluence:collection := $config:app-root || '/docs';

declare function confluence:confluence($node as node(), $model as map(*), $id as xs:string) as node()* {

let $httprequestget := "<http:request method='get'><http:header name='connection' value='close' /></http:request>"
let $lastRevinDb := confluence:getLastRevInDb($id)
let $revisionInWiki := confluence:getLastRevInWiki($id)
let $revisionInWiki := if ($revisionInWiki=-1) then ($lastRevinDb) else ($revisionInWiki)

(: Only for debugging, needs to be commented out for productive state :)
(:let $lastRevinDb := 0:)

let $config-lang := string(config:get("lang.default", "multilanguage")),
    $lang := if($config-lang = "") then "" else app:getLanguage()

return
if( $revisionInWiki = $lastRevinDb )
    then (
        doc($confluence:collection || "/" || $id || '_' || $lang || '.rev' || $lastRevinDb || '.xml' )
    )
    else (
        let $login := xmldb:login("/db", "admin", "" )
        let $remove :=
            xmldb:get-child-resources( $confluence:collection )[starts-with(., $id)]
            ! xmldb:remove($confluence:collection , . )
        let $url:= config:get("confluence.url", "wiki") || $id || '?expand=body.storage'
        let $httprequestget := <http:request href="{ $url }" method="get" />
        let $export := http:send-request($httprequestget)
        let $map := parse-json(util:base64-decode($export[2]))
        let $result := map:get(map:get(map:get($map, "body"), "storage"), "value")
        let $result := local:replaceParsingErrors($result)
        let $result := '<ac:confluence xmlns:ac="http://www.atlassian.com/schema/confluence/4/ac/" xmlns:ri="http://www.atlassian.com/schema/confluence/4/ri/" xmlns="http://www.atlassian.com/schema/confluence/4/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.atlassian.com/schema/confluence/4/ac/ ">' || $result || "</ac:confluence>"
        let $result as element() := parse-xml( $result )/*
        let $result := local:remove-element-ns-deep( $result )
        let $result := <div id="wiki">{local:confluenceparser($result, $id)}</div>

        let $allLanguages := app:getAllLanguages()
        let $forLoop := map:merge(
            for $el in $allLanguages
                let $resultLangSpecific := local:confluenceparseLanguage($result, $el)
                let $store := xmldb:store( $confluence:collection, $id || '_' || $el || '.rev' || $revisionInWiki || '.xml' , $resultLangSpecific )
                return map:entry($el, $store)
            )

        let $result := doc( $forLoop( $lang ) )
        return ($result)
    )

};

(:~
 : Takes the multilanguage output of Confluence (element(structured-macro))
 : and transforms the output to language related div.
 :
 : @param $nodes – confluence node(s)
 : @param $lang – the target language
 :  :)
declare function local:confluenceparseLanguage($nodes as node()*, $lang){

for $node in $nodes return
    typeswitch($node)
        case element ( structured-macro ) return
                if( string($node/@name) = (config:get("confluence.lang", "wiki")[@code=$lang]/string(@name)))
                then
                    element xhtml:div {
                        local:confluenceparseLanguage($node/rich-text-body/node(), $lang)
                    }
                else ()
(:                    error( QName("https://sade.textgrid.de/ns/wiki", "CONF02"), "Language ("|| $lang ||") not supported. Request was: "||$node/@name):)
        case element( * ) return
            element {local-name($node)} {
                $node/@*,
                local:confluenceparseLanguage($node/node(), $lang)
            }
        case text() return $node
        default return local:confluenceparseLanguage($node/node(), $lang)
};

declare function local:confluenceparser($nodes as node()*, $id){
for $node in $nodes return
    typeswitch($node)
        (:Confluence often returns an empty p element at the beginning:)
        case element (p) return
            switch ($node/@class)
            case "auto-cursor-target"
                return ()
            default
                return
                element {local-name($node)} {
                    attribute test { $node/parent::*/child::*[1] = $node },
                    $node/@*,
                    local:confluenceparser($node/node(), $id)
                }
    (:
        case element (p) return
            element xhtml:p {
                attribute class {"lead"},
                for $att in $node/@*
                    let $att-name := name($att)
                    return if ($att-name != "class") then attribute {$att-name} {$att}
                    else (),
                local:confluenceparser($node/node(), $id)
            }:)
        case element ( image ) return
            element xhtml:img {
                attribute src { "https://wiki.de.dariah.eu/download/attachments/" || $id || "/" || ($node/attachment/@filename) },
                attribute alt { ($node/@title) },
                attribute height { ($node/@height) }
                (: attribute width { ($node/@width) } ---->: Width can't be set in Confluence! :)
            }
        (:Transform heading h1 to the theme specific style:)
        case element ( h1 ) return
            element xhtml:h1 {
            attribute class {"block-header"},
                local:confluenceparser($node/node(), $id)
            }
        case element ( h2 ) return
            element xhtml:h2 {
            attribute class {"block-header alt"},
                local:confluenceparser($node/node(), $id)
            }
        (: Structured Macros of Confluence needs to be transformed :)
        case element ( structured-macro ) return
            if(matches($node/@name, "^[a-zA-Z]"))
            then (
                switch ($node/@name)
                    case "info"
                        return
                            element xhtml:div {
                                attribute class {"info-board info-board-primary"},
                                local:confluenceparser($node/rich-text-body/node(), $id)
                            }
                    case "warning"
                        return
                            element xhtml:div {
                                attribute class {"alert alert-danger"},
                                local:confluenceparser($node/rich-text-body/node(), $id)
                            }
                    case "tip"
                        return
                            element xhtml:div {
                                attribute class {"alert alert-success"},
                                local:confluenceparser($node/rich-text-body/node(), $id)
                            }
                    case "code"
                        return
                            element xhtml:code {
                                local:confluenceparser($node/plain-text-body/node(), $id)
                            }
                    default
                        return
                            if( string($node/@name) = (config:get("confluence.lang", "wiki")/string(@name))[.!=""])
                            then
                                element {local-name($node)} {
                                    $node/@*,
                                    local:confluenceparser($node/node(), $id)
                                }
                            else error( QName("https://sade.textgrid.de/ns/wiki", "CONF03"), "Structured macro ("|| string($node/@name) ||") unknown to the parser.")
            )
            else error( QName("https://sade.textgrid.de/ns/wiki", "CONF01"), "Structured Macro has no valid name.")
        case element ( table ) return
            element table {
                attribute class {"table table-responsive"},
                $node/(@* except @class),
                local:confluenceparser($node/node(), $id)
            }
        (: Edit links to open external ones in a new tab :)
        case element ( a ) return
            element a {
                if (starts-with($node/@href,"https://wiki.de.dariah.eu/")) then (
                    attribute href {functx:substring-after-if-contains($node/@href,"https://wiki.de.dariah.eu/")},
                    $node/text())
                else (
                    (:attribute href {$node/@href},:)
                    attribute target {"_blank"},
                    $node/@*,
                    local:confluenceparser($node/node(), $id))
            }
        case element( * ) return
            element {local-name($node)} {
                $node/@*,
                local:confluenceparser($node/node(), $id)
            }
        case text() return $node
        default return local:confluenceparser($node/node(), $id)
};

declare function local:passthru($node as node()*, $id) as item()* {
    element {name($node)} {($node/@*, local:confluenceparser($node/node(), $id))}
};

(:  not needed anymore, but could be handy for further usage :)
(: httpclient-syntax deprecated
declare function local:retrieveImageUrl($pageId, $imageName) {
       let $url:= xs:anyURI('https://wiki.de.dariah.eu/rest/api/content/' || $pageId || '/child/attachment?filename=' || encode-for-uri($imageName)),
            $persist := false(),
            $request-headers:= <headers></headers>,
            $export := httpclient:get(xs:anyURI($url), false(), $request-headers),
            $map := parse-json ( util:base64-decode(
            string(xs:base64Binary($export//httpclient:body/text())) ) ),
            $imageurl := $map("results")(1)("_links")("download")
        return 'https://wiki.de.dariah.eu/' || $imageurl
};
:)

declare function confluence:getLastRevInDb($id) as xs:integer {
let $num := xmldb:get-child-resources( $confluence:collection )[contains(., $id)][1]
    => substring-after('.rev')
    => substring-before('.xml')

return
    if($num = "")
    then 0
    else $num
};

(:~
 : Returns the last revision number we get from Confluence
 : @param $id – the ID of a Confluence page
 :  :)
declare function confluence:getLastRevInWiki($id as xs:string) as xs:integer {
    let $url := config:get("confluence.url", "wiki")||$id||"/history?expand=lastUpdated"
    let $httprequestget := <http:request href="{ $url }" method="get" />
    let $rest := http:send-request($httprequestget)
    let $statuscode := $rest[1]/@status

    let $map := if ($statuscode = 200) then (
        parse-json(util:base64-decode($rest[2]))
    )
    else ()

    let $revision := if ($statuscode = 200) then (
        string(map:get (map:get($map, "lastUpdated"), "number"))
    )
    else (-1)

    return
        $revision
};

declare function local:HTML2html($node) {
for $node in $node
return
    typeswitch ( $node )
    case element() return element { lower-case($node/local-name()) } { $node/@*, local:HTML2html($node/node()) }
    default return $node
};

declare function local:remove-element-ns-deep( $nodes as node()*)  as node()* {
  for $node in $nodes
  return if ($node instance of element())
         then (element
               {QName ("", local-name($node))}
               {
                   for $attr in $node/@*
                   return
                       attribute { local-name($attr) } { string($attr) },
                   local:remove-element-ns-deep($node/node())})
         else if ($node instance of document-node())
         then local:remove-element-ns-deep($node/node())
         else $node
};

declare function local:replaceParsingErrors($result) {
    (: Strip HTML comments, causes sometimes error :)
    let $temp := replace($result, "<!--.*?-->", ""),
    (: two dashes inside ac macros also causes an error, needs better fix :)
    $temp := replace($temp, "--", "&#xFE58;&#xFE58;"),
    (: Returns cause errors :)
    $temp := replace($temp, "&amp;nbsp;", " "),
    (: ß cause errors :)
    $temp := replace($temp, "&amp;szlig;","ß"),
    (: Umlauts cause errors :)
    $temp := replace($temp, "&amp;uuml;", "ü"),
    $temp := replace($temp, "&amp;auml;", "ä"),
    $temp := replace($temp, "&amp;ouml;", "ö"),
    $temp := replace($temp, "&amp;Uuml;", "Ü"),
    $temp := replace($temp, "&amp;Auml;", "Ä"),
    $temp := replace($temp, "&amp;Ouml;", "Ö"),
    (: French special chars cause errors :)
    $temp := replace($temp, "&amp;eacute;","é"),
    $temp := replace($temp, "&amp;egrave;","è"),
    $temp := replace($temp, "&amp;Egrave;","È"),
    $temp := replace($temp, "&amp;Eacute;","É"),
    $temp := replace($temp, "&amp;yuml;","ù"),
    $temp := replace($temp, "&amp;Yuml;","Û"),
    $temp := replace($temp, "&amp;ucirc;","û"),
    $temp := replace($temp, "&amp;Ucirc;","Û"),
    $temp := replace($temp, "&amp;Ecirc;","Ê"),
    $temp := replace($temp, "&amp;ecirc;","ê"),
    $temp := replace($temp, "&amp;Euml;","Ë"),
    $temp := replace($temp, "&amp;euml;","ë"),
    $temp := replace($temp, "&amp;Icirc;","Î"),
    $temp := replace($temp, "&amp;icirc;","î"),
    $temp := replace($temp, "&amp;Iuml;","Ï"),
    $temp := replace($temp, "&amp;iuml;","ï"),
    $temp := replace($temp, "&amp;Ocirc;","Ô"),
    $temp := replace($temp, "&amp;ocirc;","ô"),
    $temp := replace($temp, "&amp;OElig;","Œ"),
    $temp := replace($temp, "&amp;oelig;","œ"),
    $temp := replace($temp, "&amp;Ugrave;","Ù"),
    $temp := replace($temp, "&amp;ugrave;","ù"),
    $temp := replace($temp, "&amp;Agrave;","À"),
    $temp := replace($temp, "&amp;agrave;","à"),
    $temp := replace($temp, "&amp;Acirc;","Â"),
    $temp := replace($temp, "&amp;acirc;","â"),
    $temp := replace($temp, "&amp;AElig;","Æ"),
    $temp := replace($temp, "&amp;aelig;","æ"),
    $temp := replace($temp, "&amp;Ccedil;","Ç"),
    $temp := replace($temp, "&amp;ccedil;","ç"),
    $temp := replace($temp, "&amp;Oacute;","Ó"),
    $temp := replace($temp, "&amp;oacute;","ó"),
    $temp := replace($temp, "&amp;aacute;","á"),
    $temp := replace($temp, "&amp;Aacute;","Á"),
    $temp := replace($temp, "&amp;Eacute;","É"),
    $temp := replace($temp, "&amp;eacute;","é"),
    $temp := replace($temp, "&amp;Uacute;","Ú"),
    $temp := replace($temp, "&amp;uacute;","ú"),
    $temp := replace($temp, "&amp;Iacute;","Í"),
    $temp := replace($temp, "&amp;iacute;","í"),
    $temp := replace($temp, "&amp;aring;","å"),
    $temp := replace($temp, "&amp;Aring;","Å"),

    (:Other special characters:)
    $temp := replace($temp, "&amp;deg;","°"),
    $temp := replace($temp, "&amp;rsaquo;","›"),
    $temp := replace($temp, "&amp;lsaquo;","‹"),
    $temp := replace($temp, "&amp;rsquo;","’"),
    $temp := replace($temp, "&amp;lsquo;","‘"),
    $temp := replace($temp, "&amp;ndash;","—"),
    $temp := replace($temp, "&amp;mdash;","—"),
    $temp := replace($temp, "&amp;laquo;","«"),
    $temp := replace($temp, "&amp;raquo;","»"),
    $temp := replace($temp, "&amp;ldquo;","“"),
    $temp := replace($temp, "&amp;rdquo;","”"),
    $temp := replace($temp, "&amp;bdquo;","„"),
    $temp := replace($temp, "&amp;hellip;","..."),
    $temp := replace($temp, "&amp;times;","x"),
    $temp := replace($temp, "&amp;sect;","§"),
    $temp := replace($temp, "&amp;acute;","´"),
    $temp := replace($temp, "&amp;sbquo;","‚"),
    $temp := replace($temp, "&amp;dagger;","†"),
    $temp := replace($temp, "&amp;euro;","€"),
    $temp := replace($temp, "&amp;shy;","–")

    return $temp
};

declare function functx:substring-after-if-contains
  ( $arg as xs:string? ,
    $delim as xs:string )  as xs:string? {

   if (contains($arg,$delim))
   then substring-after($arg,$delim)
   else $arg
 };

ARG version
ARG build_dir

FROM debian:buster-slim AS builder

RUN apt-get update && apt-get install zip -yy

ARG build_dir
COPY src/ ${build_dir}
WORKDIR ${build_dir}

ARG version
RUN sed -i "s/@version@/${version}/g" expath-pkg.xml
RUN zip -r conpa-${version}.xar *


FROM existdb/existdb:5.2.0

WORKDIR /exist

ENTRYPOINT ["java"]
CMD ["org.exist.start.Main", "jetty"]

ARG version
ARG build_dir
COPY --from=builder ${build_dir}conpa-${version}.xar /exist/autodeploy
